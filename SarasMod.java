package com.saramae.tutorial;

import org.apache.logging.log4j.Logger;

import com.saramae.tutorial.proxy.CommonProxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = SarasMod.MODID, name = SarasMod.MODNAME, version = SarasMod.VERSION, dependencies = "required-after:forge@[11.16.0.1865,)", useMetadata = true)
public class SarasMod {

	public static final String MODID = "sarasmod";
	public static final String MODNAME = "Mod tutorials";
	public static final String VERSION = "0.0.1";

	@SidedProxy(clientSide = "com.saramae.tutorial.proxy.ClientProxy", serverSide = "com.saramae.tutorial.proxy.ClientProxy")
	public static CommonProxy proxy;

	@Mod.Instance
	public static SarasMod instance;

	public static final SarasModTab creativeTab = new SarasModTab();
	public static Logger logger;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		MinecraftForge.EVENT_BUS.register(new LeavesDropHandler());
		proxy.preInit(event);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
		ModRecipes.init();
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}
}